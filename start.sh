#!/bin/bash

if cat /etc/hosts |grep counter-app.local > /dev/null ; then
    echo "counter-app.local  exist in your HOSTS"
else
    echo "127.0.0.1 counter-app.local" >> /etc/hosts
fi

env $(cat ./var.env)   docker-compose up -d


