import os

# Server settings
SERVER_HOST = "0.0.0.0"

SERVER_PORT = int(os.getenv("SERVER_PORT", 80))

# Redis settings
REDIS_HOST = os.environ.get("REDIS_HOST", "redis")
REDIS_PORT = int(os.getenv("REDIS_PORT", 6379))
REDIS_PWD = os.environ.get("REDIS_PWD", "pwdexample")
